# Lab10 -- Stress testing 


## Introduction

Wooo-hoo, finally some lab where you won't need to code(I'm happy as much as you). In this lab we are going to look into stress testing techniques and technologies, and test our own services until they crush. ***Let's roll!***

## Stress testing

Stress testing is needed to understand the bearable workload on your services, so your application won't crush in the middle of working day, and all your customers won't leave you. It is used to test both hardware and software parts of the product. To test mostly software you may launch stress testing locally, so you will be able to understand which requests are lagging and which parts of your product you possibly will need to improve. To test both software and infrastructure you should use external stress testing.

To perform our stress testing we are going to use artillery.io testing tool.


## Lab
 
Ok, here we go, Stress testing lab will be pretty simple though
1. Create your fork of the `
Lab10 - Stress testing
` repo and clone it. [***Here***](https://gitlab.com/sqr-inno/lab-10-stress-testing)
2. In one of the previous labs we had worked with artillery.io, so why change now? Let's use it for its original purpose - load testing. In case if you need any asistance in that, look into [here](https://artillery.io/docs/guides/guides/http-reference.html#TLS-SSL). Here is a config we are going to use today, it is really like the one we've used in our previous lab.
```yaml
config:
  target: "https://script.google.com/macros/s/{your_key}"
  phases:
    - duration: 10
      arrivalCount: 100
scenarios:
  - name: "Fuzz some stuff"
    flow:
      - get:
          url: "/exec?service=calculatePrice&email=golden&type=luxury&plan=minute&distance=100&planned_distance=100&time=110&planned_time=100&inno_discount=yes"
```
To run artillery with generating report we need to execute:
```sh
artillery run -o result.json ./example.yaml
```
Afterwards we will be able to visualise it, by:
```sh
artillery report result.json
```
Let's make it a bit more interesting, usually load tests consists of few phases, like Warm up, Ramp up and Constant workload, let's add them to our phases:
```yaml
 - duration: 60
      arrivalRate: 5
      name: Warm up
    - duration: 120
      arrivalRate: 5
      rampTo: 50
      name: Ramp up load
    - duration: 600
      arrivalRate: 50
      name: Sustained load
```
Plus to imitate user actions we can add one more request pause between them:
```yaml
 flow:
      - get:
          url: "/exec?service=calculatePrice&email=golden&type=luxury&plan=minute&distance=100&planned_distance=100&time=110&planned_time=100&inno_discount=yes"
      - think: 2
      - get:
          url: "/exec?service=calculatePrice&email=golden&type=luxury&plan=minute&distance=100&planned_distance=100&time=110&planned_time=100&inno_discount=yes"

```
Now let's run it once again and watch on the report.
That's it, lab is really simple. 
## Homework

As a homework you will need to open this [link](https://docs.google.com/spreadsheets/d/1yGFJgswH6nFBztaim8a4XVDAiyoco7OgVoNaryLVW7E/edit?usp=sharing), load test your service and provide the results of your test as a screenshot attached to the readme file with Artillery. Please establish how much rps you can get for (GetSpec, CalculatePrice with fixed_price plan, CalculatePrice with minute plan) and provide screenshots of the report and load testing execution(with user in bash shell) as well as your `test.yaml` file. 
**Lab is counted as done, if pipelines are passing. and tests are developped**

## Results

`email: a.martyanova@innopolis.university`

![](img/html.png)

![](img/summary.png)

